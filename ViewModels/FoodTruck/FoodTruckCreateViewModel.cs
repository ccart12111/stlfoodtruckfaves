﻿using STLFoodTruckFaves.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.ViewModels.FoodTruck
{
    public class FoodTruckCreateViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
        
        public FoodTruckCreateViewModel() { }

        public void Save(ApplicationDbContext dbContext)
        {
            Models.FoodTruck foodTruck = new Models.FoodTruck
            {
                Name = this.Name,
                Description = this.Description
            };

            dbContext.Add(foodTruck);
            dbContext.SaveChanges();
        }



    }
}
