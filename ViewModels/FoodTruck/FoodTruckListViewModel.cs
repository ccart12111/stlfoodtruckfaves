﻿using STLFoodTruckFaves.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.ViewModels.FoodTruck
{
    public class FoodTruckListViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        //TODO: add Category BO
        //public string Category { get; set; }
        public string Description { get; set; }



        //gets List of all FoodTruck BOs from the database directly - we don't have a data layer here
        //and puts them in a list of FoodTruckListViewModels for use in the view

        public static List<FoodTruckListViewModel> GetFoodTruckListViewModels(ApplicationDbContext dbContext)
        {
            List<Models.FoodTruck> foodTrucks = dbContext.FoodTrucks.ToList();

            List<FoodTruckListViewModel> foodTruckListViewModels = new List<FoodTruckListViewModel>();
            foreach (Models.FoodTruck foodTruck in foodTrucks)
            {
                FoodTruckListViewModel viewModel = new FoodTruckListViewModel();
                viewModel.ID = foodTruck.ID;
                viewModel.Name = foodTruck.Name;
                viewModel.Description = foodTruck.Description;

                foodTruckListViewModels.Add(viewModel);
            }
            return foodTruckListViewModels;
        }

    }

}
