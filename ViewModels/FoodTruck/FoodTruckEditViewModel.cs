﻿using STLFoodTruckFaves.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.ViewModels.FoodTruck
{
    public class FoodTruckEditViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public FoodTruckEditViewModel() { }

        public FoodTruckEditViewModel (ApplicationDbContext dbContext, int iID)
        {
            FoodTruckEditViewModel model = new FoodTruckEditViewModel();
            Models.FoodTruck foodTruckToEdit = dbContext.FoodTrucks.Single(f => f.ID == iID);
           
            Name = foodTruckToEdit.Name;
            Description = foodTruckToEdit.Description;
        }

        public void Update(ApplicationDbContext dbContext, int iID)
        {
            Models.FoodTruck foodTruck = new Models.FoodTruck
            {
                ID = iID,
                Name = this.Name,
                Description = this.Description
            };

            dbContext.Update(foodTruck);
            dbContext.SaveChanges();
        }
    }
}
