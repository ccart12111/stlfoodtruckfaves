﻿using STLFoodTruckFaves.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.ViewModels.FoodTruck
{
    public class FoodTruckDetailsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public FoodTruckDetailsViewModel() { }

        public FoodTruckDetailsViewModel(ApplicationDbContext dbContext, int iID)
        {
            FoodTruckDetailsViewModel model = new FoodTruckDetailsViewModel();
            Models.FoodTruck foodTruck = dbContext.FoodTrucks.Single(f => f.ID == iID);

            Name = foodTruck.Name;
            Description = foodTruck.Description;

        }

    }
}
