﻿using Microsoft.AspNetCore.Mvc;
using STLFoodTruckFaves.Data;
using STLFoodTruckFaves.ViewModels.FoodTruck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.Controllers
{
    public class FoodTruckController : Controller
    {
        private ApplicationDbContext dbContext;

        public FoodTruckController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            List<FoodTruckListViewModel> models = FoodTruckListViewModel.GetFoodTruckListViewModels(dbContext);

            //pass this list of view models into the view
            return View(models);
            //test comment
            //another test commmit
            //test to merge
            //test for staging
        }

        //create an instance of FoodTruckCreateViewModel and return the Create View, passing it in to the View
        [HttpGet]
        public IActionResult CreateFoodTruck()
        {
            
            FoodTruckCreateViewModel model = new FoodTruckCreateViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateFoodTruck(FoodTruckCreateViewModel model)
        {
            model.Save(dbContext);
            return RedirectToAction(actionName: nameof(Index));
        }

        public IActionResult FoodTruckDetails(int ID)
        {
            return View(model: new FoodTruckDetailsViewModel(dbContext, ID));
        }

        [HttpGet]
        public IActionResult EditFoodTruck(int ID)
        {
            //FoodTruckEditViewModel model = GetFoodTruckToEdit(dbContext, ID);
           
            return View(model: new FoodTruckEditViewModel(dbContext, ID));
        }

        [HttpPost]
        public IActionResult EditFoodTruck(FoodTruckEditViewModel model, int ID)
        {
            model.Update(dbContext, ID);
            return RedirectToAction(actionName: nameof(Index));
        }

        public IActionResult DeleteFoodTruck(FoodTruckListViewModel model, int ID)
        {
            //find the FoodTruck bo in the db by the id (if they match) and then delete that foodTruck in the db
            //create a list of FoodTrucks bo's, then find the one that matches the id passed in

            List<Models.FoodTruck> foodTrucks = dbContext.FoodTrucks.ToList();
            Models.FoodTruck foodTruckToDelete = foodTrucks.Single(f => f.ID == ID);

            
            //maybe need to check if more than one and add a validation message
            //List<Models.FoodTruck> foodTrucksList = dbContext.FoodTrucks.Where(f => f.ID == ID).ToList();

            //then remove that FoodTruck bo from the database
            dbContext.Remove(foodTruckToDelete);
            dbContext.SaveChanges();
            return RedirectToAction(actionName: nameof(Index));
        }

       
    }
}
