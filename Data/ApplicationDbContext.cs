﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using STLFoodTruckFaves.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace STLFoodTruckFaves.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<FoodTruck> FoodTrucks { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
