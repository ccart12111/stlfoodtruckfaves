﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.Models
{
    public class MainDish
    {
        public int ID { get; set; }
        public string MainDishDescription { get; set; }

        public string MainDishPrice { get; set; }

        //foreign key, 1 to many; one Menu can have many MainDishes
        public int MenuID { get; set; }

        //reference or navigation property?

        public Menu Menu { get; set; }
    }
}
