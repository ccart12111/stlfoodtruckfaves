﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STLFoodTruckFaves.Models
{
    public class Menu
    {
        public int ID { get; set; }
        public string MenuDescription { get; set; }

        public List<MainDish> MainDishes { get; set; }

    }
}
